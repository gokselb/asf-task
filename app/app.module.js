'use strict';

angular.module('asfTask', [
    'asfTask.header',
    'asfTask.home',
    'asfTask.segment',
    'asfTask.guestbook',
    'asfTask.footer',
    'asfTask.services'
])

