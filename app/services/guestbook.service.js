'use strict';

angular.module('asfTask.services', []).service('GuestBookService', function () {
  this.getAll = () => {
    var messages = angular.fromJson(sessionStorage.GuestBookService);
    if (!messages) {
      messages = [];
    }
    messages.sort(function (a, b) {
      return new Date(b.date) - new Date(a.date);
    });
    return messages;
  }

  this.save = (messages) => {
    sessionStorage.GuestBookService = angular.toJson(messages);
  }
});