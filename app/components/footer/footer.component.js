'use strict';

angular.module('asfTask.footer', [])
  .component('footerComponent', {
    templateUrl: 'components/footer/footer.template.html',

    controller: function ($scope) {
      // For copyright 
      $scope.year = new Date();
    }
  });

