'use strict';

angular.module('asfTask.header', [])
  .component('headerComponent', {
    templateUrl: 'components/header/header.template.html',
    controller: function ($scope) {
      $scope.current = "home";
    }
  });

