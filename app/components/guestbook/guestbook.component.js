'use strict';



angular.module('asfTask.guestbook', [])
  .component('guestbookComponent', {
    templateUrl: 'components/guestbook/guestbook.template.html',
    controller: function ($scope, GuestBookService) {
      // Get notes from DB
      var messages = [];
      var getAll = () => {
        $scope.newMessage = {
          name: "",
          date: new Date(),
          subject: "",
          body: ""
        }
        messages = GuestBookService.getAll();
        $scope.messages = messages;

      }
      getAll();



      // Save notes to DB
      $scope.save = (newMessage) => {
        newMessage.date = new Date();
        messages.unshift(newMessage);
        GuestBookService.save(messages);
        getAll();
      }
    }
  });

